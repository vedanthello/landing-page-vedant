# Task
 Build out the landing page mentioned in the GitLab's repo whose link is: https://gitlab.com/mountblue/cohort-12-javascript-2/html-css-landing-page, for both mobile and desktop.

# Notes:
1. For mobile version, the screen width is 375px, whereas, for desktop version, the screen width is 1440px.
2. Get the landing page look as close as possible.
3. Users should be able to see hover states for all interactive elements on the page.

# Areas for improvement:
1. Uniform spacing in header for desktop design
2. Hover states for social icons
3. Navbar in the header for desktop design should always be visible
4. Background patterns for both mobile and desktop design