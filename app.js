require('dotenv').config();
const config = require("./config.js");

const express = require("express");
const app = express();

// serve index.html file
app.get("/", (req, res) => {
  res.sendFile("index.html", {root: __dirname}, err => {
    if(err) {
      res.sendStatus(500);
    } else {
      console.log("Sent index.html file");
    }
  });
});

// serve static files
app.use(express.static("assets"));

app.listen(config.port, () => {
  console.log(`Server started on port ${config.port}`);  
});