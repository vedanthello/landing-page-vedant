const wrapper = document.getElementById("menu-icon-close-icon-wrapper");
const nav = document.getElementsByTagName("nav")[0];
const menuIcon = document.getElementById("menu-icon");

wrapper.addEventListener("click", event => {
  if (nav.style.display === "block") {
    nav.style.display = "none";
    menuIcon.style.visibility = "visible";
  } else {
    nav.style.display = "block";
    menuIcon.style.visibility = "hidden";
  }
});
  